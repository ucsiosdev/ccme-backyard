//
//  MobileNumberSearchViewController.m
//  CCMe
//
//  Created by appledev on 18/02/19.
//  Copyright © 2019 appledev. All rights reserved.
//

#import "MobileNumberSearchViewController.h"
#import "IQKeyboardManager.h"
#import "COIDefaults.h"
#import "COIDefaultSearchViewController.h"
#import "FormViewController.h"
#import "ChildrenFormViewController.h"
#import "MBProgressHUD.h"
#import "FormViewController.h"
#import "COINetworkManager.h"

@interface MobileNumberSearchViewController ()

@end

@implementation MobileNumberSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark UITextFieldDelegateMethods
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    IQKeyboardManager *keyboardManager = [IQKeyboardManager sharedManager];
    if (keyboardManager.canGoNext) {
        [keyboardManager goNext];
    } else {
        [keyboardManager resignFirstResponder];
    }
    return NO;
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;   // return NO to not change text
{
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    if([string length] == 0)
    {
        if([textField.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if(textField.text.length==0 && [string isEqualToString:@" "])
    {
        return NO;
    }
    if( textField == mobileNumberTextField &&  textField.text.length == 0 && string.length!=0){
        if([[string substringToIndex:1] isEqualToString:@"0"]){
            return NO;
        }
    }
    if(textField == mobileNumberTextField)
    {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSString *expression = @"^[0-9]*$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return ((numberOfMatches != 0) && newLength<=10);
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if(textField == countryCodeTextField && countryCodeTextField.textState== Error){
        [countryCodeTextField setTextState: normal];
    }else if(textField == mobileNumberTextField && mobileNumberTextField.textState== Error){
        [mobileNumberTextField setTextState: normal];
    }
    
    if (textField == countryCodeTextField) {
        [self resignKeyboard];
        
        if (nationalitiesDataArray.count == 0) {
            [self getNationalitiesData];
        } else {
            [self showDropDownList:nationalitiesDataArray withTitle:@"Select Country Code" inView:textField];
        }        
        return NO;
    }

    return YES;
}

-(void)resignKeyboard {
    if ([mobileNumberTextField isFirstResponder]) {
        [mobileNumberTextField resignFirstResponder];
    }
}

-(void)showDropDownList:(NSMutableArray *)dropDownArray withTitle:(NSString *)titleString inView:(UITextField*)textfield {
    if (dropDownArray.count>0) {
        
        COIDefaultSearchViewController *defaultSearchVC = [[COIDefaultSearchViewController alloc]init];
        defaultSearchVC.searchDelegate = self;
        defaultSearchVC.contentArray = dropDownArray;
        defaultSearchVC.objectType = @"CountryCode";
        defaultSearchVC.titleString = titleString;
        
        UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:defaultSearchVC];
        baNavController.preferredContentSize=CGSizeMake(376, 400);
        baNavController.modalPresentationStyle = UIModalPresentationPopover;
        UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
        presentationController.delegate=self;
        presentationController.sourceView = textfield;
        presentationController.sourceRect = CGRectMake(0, 0, textfield.frame.size.width, textfield.frame.size.height);
        presentationController.permittedArrowDirections = UIPopoverArrowDirectionAny;
        
        [self presentViewController:baNavController animated:YES completion:^{
            presentationController.passthroughViews=nil;
            self.modalInPopover=YES;
            
        }];
    }
}

#pragma mark dropdown view delegate method
-(void)selectedObject:(id)selectedItem
{
    countryCodeTextField.text = [selectedItem valueForKey:@"DialCode"];
    mobileNumberTextField.text = @"";
}


- (IBAction)searchButtonTapped:(id)sender {
    [self.view endEditing:YES];
    
    if (![[COINetworkManager retrieveManager] isNetworkReachable]) {
        [COIDefaults ShowAlert:@"No network" andMessage:@"Please check your network connection" withController:self];
    }
    else if(countryCodeTextField.text.length==0 || countryCodeTextField.textState == Error)
    {
        [countryCodeTextField setTextState:Error];
        [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please select country code" withController:self];
    }
    else if(mobileNumberTextField.text.length==0 || mobileNumberTextField.textState == Error){
        [mobileNumberTextField setTextState:Error];
        [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please enter mobile number" withController:self];
    }
    else{
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        NSString *mobileNumber = [countryCodeTextField.text stringByAppendingString:mobileNumberTextField.text];
        NSString *currentTimeStamp = [COIDefaults GetCurrentTimeStamp];
        NSString *hashParam  = [NSString stringWithFormat:@"MobileNo=%@&RequestTimeStamp=%@&AppKey=%@",mobileNumber, currentTimeStamp,KAppKey];
        hashParam = [hashParam stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString *checkSum = [COIDefaults hmacWithKey:KPrivateKey andData:hashParam];
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@svc/svcHifi.svc/SearchCustomer?MobileNo=%@&RequestTimeStamp=%@&AppKey=%@&TokenID=%@",KWebServiceBaseURL,[COIDefaults getUTF8String:mobileNumber],[COIDefaults getUTF8String:currentTimeStamp],[COIDefaults getUTF8String:KAppKey],[COIDefaults getUTF8String:checkSum]]];
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        configuration.timeoutIntervalForResource = kWebServiceTimeOutInterval;
        configuration.timeoutIntervalForRequest = kWebServiceTimeOutInterval;
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:kWebServiceTimeOutInterval];
        
        // Specify that it will be a POST request
        request.HTTPMethod = @"GET";
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
            if(data!=nil &&httpResponse.statusCode==200){
                
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                NSLog(@"search customer response is %@",json);
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    if([[json valueForKey:@"ResponseCode"] isEqualToString:@"Y"] || [[json valueForKey:@"ProcessResponse"] isEqualToString:@"Record not found"]){
                        
                        NSString *custID = [NSString getValidStringValue:[json valueForKey:@"CustID"]];
                        if ([custID isEqualToString:@""]) {
                            FormViewController *formVC = [[FormViewController alloc]initWithNibName:@"FormViewController" bundle:nil];
                            [self.navigationController pushViewController:formVC animated:YES];
                        }
                        else{
                            ChildrenFormViewController *childFormVC = [[ChildrenFormViewController alloc]initWithNibName:@"ChildrenFormViewController" bundle:nil];
                            childFormVC.custID = [json valueForKey:@"CustID"];
                            [self.navigationController pushViewController:childFormVC animated:YES];
                        }
                    }else{
                        [COIDefaults ShowAlert:@"Error" andMessage:[json valueForKey:@"ProcessResponse"] withController:self];
                    }
                });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    // Server Failure
                });
            }
        }];
        [postDataTask resume];
    }
}

-(void)getNationalitiesData {
    
    NSString *currentTimeStamp = [COIDefaults GetCurrentTimeStamp];
    
    NSString *hashParam  =[NSString stringWithFormat:@"RequestTimeStamp=%@&AppKey=%@",currentTimeStamp, KAppKey];
    hashParam = [hashParam stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *result =[COIDefaults hmacWithKey:KPrivateKey andData:hashParam];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@svc/svcHifi.svc/GetNationality?RequestTimeStamp=%@&AppKey=%@&TokenID=%@",KWebServiceBaseURL, [COIDefaults getUTF8String:currentTimeStamp], [COIDefaults getUTF8String:KAppKey], [COIDefaults getUTF8String:result]]];
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.timeoutIntervalForResource = kWebServiceTimeOutInterval;
    configuration.timeoutIntervalForRequest = kWebServiceTimeOutInterval;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWebServiceTimeOutInterval];
    // Specify that it will be a GET request
    request.HTTPMethod = @"GET";
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if(data!=nil &&httpResponse.statusCode==200){
            
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSLog(@"Nationality Data is %@",json);
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if([[json valueForKey:@"ResponseCode"] isEqualToString:@"Y"]){
                    
                    self->nationalitiesDataArray = [[json valueForKey:@"ListNationalityContract"] mutableCopy];
                    if (self->nationalitiesDataArray.count > 0) {
                        // remove --Select-- object from list
                        [self->nationalitiesDataArray removeObjectAtIndex:0];
                    }
                    
                    if (self->nationalitiesDataArray.count>0) {
                        [COIDefaults setNationalityArray:self->nationalitiesDataArray];
                        [self showDropDownList:self->nationalitiesDataArray withTitle:@"Select Country Code" inView:self->countryCodeTextField];
                    } else {
                        [COIDefaults ShowAlert:@"No Data" andMessage:@"Nationality Data not found." withController:self];
                    }
                    
                }else{
                    [COIDefaults ShowAlert:@"Error" andMessage:[json valueForKey:@"ProcessResponse"] withController:self];
                }
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                // Server Failure
            });
        }
    }];
    [postDataTask resume];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
