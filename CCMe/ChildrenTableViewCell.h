//
//  ChildrenTableViewCell.h
//  CCMe
//
//  Created by Neha Gupta on 22/02/19.
//  Copyright © 2019 appledev. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChildrenTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@end

NS_ASSUME_NONNULL_END
