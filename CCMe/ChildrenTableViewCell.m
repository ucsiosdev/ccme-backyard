//
//  ChildrenTableViewCell.m
//  CCMe
//
//  Created by Neha Gupta on 22/02/19.
//  Copyright © 2019 appledev. All rights reserved.
//

#import "ChildrenTableViewCell.h"

@implementation ChildrenTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
