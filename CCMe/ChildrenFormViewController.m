//
//  ChildrenFormViewController.m
//  CCMe
//
//  Created by appledev on 19/02/19.
//  Copyright © 2019 appledev. All rights reserved.
//

#import "ChildrenFormViewController.h"
#import "IQKeyboardManager.h"
#import "COIDefaults.h"
#import "ThankYouViewController.h"
#import "MBProgressHUD.h"
#import "MobileNumberSearchViewController.h"
#import "COIDefaultSearchViewController.h"
#import "COINetworkManager.h"
#import "ChildrenTableViewCell.h"

@interface ChildrenFormViewController ()<UIPopoverPresentationControllerDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@end

@implementation ChildrenFormViewController

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define IS_IPAD_PRO_1366 (IS_IPAD && MAX(SCREEN_WIDTH,SCREEN_HEIGHT) == 1366.0)
#define IS_IPAD_PRO_1024 (IS_IPAD && MAX(SCREEN_WIDTH,SCREEN_HEIGHT) == 1024.0)

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    arrChildrenName = [[NSMutableArray alloc]init];
    
    if (IS_IPAD_PRO_1366)
    {
        //its ipad Pro 12.9 inch screen
        
        xFormViewTopConstraint.constant = 150;
    } else {
        xFormViewTopConstraint.constant = 80;
    }
    
    childrenNameTableView.layer.borderColor = TextFieldBorderColor.CGColor;
    childrenNameTableView.layer.masksToBounds = YES;
    childrenNameTableView.layer.borderWidth = 1.5;
    [childrenNameTableView registerNib:[UINib nibWithNibName:@"ChildrenTableViewCell" bundle:nil] forCellReuseIdentifier:@"cellIdentifier"];
    
    btnAddChild.userInteractionEnabled = NO;
    btnAddChild.alpha = 0.5;
    
    childrenNameTextField.userInteractionEnabled = NO;
    childrenNameTextField.alpha = 0.5;
}

#pragma mark UITextFieldDelegateMethods
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    IQKeyboardManager *keyboardManager = [IQKeyboardManager sharedManager];
    if (keyboardManager.canGoNext) {
        [keyboardManager goNext];
    } else {
        [keyboardManager resignFirstResponder];
    }
    return NO;
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;   // return NO to not change text
{
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    if([string length] == 0)
    {
        if([textField.text length] != 0)
        {
            if (textField == numberOfChildrenTextField && [textField.text length] == 1) {

                btnAddChild.userInteractionEnabled = NO;
                btnAddChild.alpha = 0.5;
                    
                childrenNameTextField.userInteractionEnabled = NO;
                childrenNameTextField.alpha = 0.5;
            }
            return YES;
        }
        else {
            return NO;
        }
    }
    if(textField.text.length==0 && [string isEqualToString:@" "])
    {
        return NO;
    }
    if( textField == numberOfChildrenTextField &&  textField.text.length == 0 && string.length!=0){
        if([[string substringToIndex:1] isEqualToString:@"0"]){
            return NO;
        }
    }
    if(textField == numberOfChildrenTextField)
    {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSString *expression = @"^[0-9]*$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        
        if ((numberOfMatches != 0) && newLength<=2) {
            btnAddChild.userInteractionEnabled = YES;
            btnAddChild.alpha = 1.0;

            childrenNameTextField.userInteractionEnabled = YES;
            childrenNameTextField.alpha = 1.0;
        }
        
        return ((numberOfMatches != 0) && newLength<=2);
    }
    else if(textField == childrenNameTextField || textField == childrenNameTextField)
    {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSString *expression = @"^[A-Za-z ]*$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return ((numberOfMatches != 0) && newLength<=50);
    }
    else if(newLength>50)
    {
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if(textField == eventListTextField && eventListTextField.textState== Error){
        [eventListTextField setTextState:normal];
    } else if(textField == numberOfChildrenTextField && numberOfChildrenTextField.textState== Error){
        [numberOfChildrenTextField setTextState: normal];
    }else if(textField == childrenNameTextField && childrenNameTextField.textState== Error){
        [childrenNameTextField setTextState: normal];
    }
    
    if (textField == eventListTextField) {
        [self resignKeyboard];
        
        if (arrayOfEventList.count == 0) {
            [self getEventList];
        } else {
            COIDefaultSearchViewController *defaultSearchVC = [[COIDefaultSearchViewController alloc]init];
            defaultSearchVC.searchDelegate = self;
            defaultSearchVC.contentArray = arrayOfEventList;
            defaultSearchVC.objectType = @"Event List";
            defaultSearchVC.titleString = @"Select Event List";
            
            UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:defaultSearchVC];
            baNavController.preferredContentSize=CGSizeMake(376, 400);
            baNavController.modalPresentationStyle = UIModalPresentationPopover;
            UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
            presentationController.delegate=self;
            presentationController.sourceView = eventListTextField;
            presentationController.sourceRect = CGRectMake(0, 0, eventListTextField.frame.size.width, eventListTextField.frame.size.height);
            presentationController.permittedArrowDirections = UIPopoverArrowDirectionAny;
            
            [self presentViewController:baNavController animated:YES completion:^{
                presentationController.passthroughViews=nil;
                self.modalInPopover=YES;
            }];
        }
        return NO;
    }
    if (textField == childrenNameTextField) {
        if (numberOfChildrenTextField.text.intValue == arrChildrenName.count) {
            btnAddChild.userInteractionEnabled = NO;
            btnAddChild.alpha = 0.5;
            
            childrenNameTextField.userInteractionEnabled = NO;
            childrenNameTextField.alpha = 0.5;
            
            [self.view endEditing:YES];
            return NO;
        }
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == numberOfChildrenTextField) {
        if (numberOfChildrenTextField.text.intValue == arrChildrenName.count) {
            btnAddChild.userInteractionEnabled = NO;
            btnAddChild.alpha = 0.5;
            
            childrenNameTextField.userInteractionEnabled = NO;
            childrenNameTextField.alpha = 0.5;
        } else if (numberOfChildrenTextField.text.intValue > arrChildrenName.count) {
            btnAddChild.userInteractionEnabled = YES;
            btnAddChild.alpha = 1.0;
            
            childrenNameTextField.userInteractionEnabled = YES;
            childrenNameTextField.alpha = 1.0;
        }
        else {
            textField.text = [NSString stringWithFormat:@"%lu",(unsigned long)arrChildrenName.count];
            [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please delete child first to update" withController:self];
            
            btnAddChild.userInteractionEnabled = NO;
            btnAddChild.alpha = 0.5;
            
            childrenNameTextField.userInteractionEnabled = NO;
            childrenNameTextField.alpha = 0.5;
        }
    }
}
-(void)resignKeyboard {
    if ([numberOfChildrenTextField isFirstResponder]) {
        [numberOfChildrenTextField resignFirstResponder];
    }
    if ([childrenNameTextField isFirstResponder]) {
        [childrenNameTextField resignFirstResponder];
    }
}
- (IBAction)submitButtonTapped:(id)sender {
    
    [self.view endEditing:YES];
    
    if (![[COINetworkManager retrieveManager] isNetworkReachable]) {
        [COIDefaults ShowAlert:@"No network" andMessage:@"Please check your network connection" withController:self];
    }
    else if(eventListTextField.text.length==0 || eventListTextField.textState == Error){
        [eventListTextField setTextState:Error];
        [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please select event" withController:self];
    }
    else if(numberOfChildrenTextField.text.length==0 || numberOfChildrenTextField.textState == Error)
    {
        [numberOfChildrenTextField setTextState:Error];
        [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please enter number of children" withController:self];
    }
    else if(numberOfChildrenTextField.text.intValue > arrChildrenName.count) {  //childrenNameTextField.text.length==0 || childrenNameTextField.textState == Error){
//        [childrenNameTextField setTextState:Error];
        
        [COIDefaults ShowAlert:@"Missing Data" andMessage:@"No. of children should be match with the entered name" withController:self];
    }
    else {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        if (_custID.length == 0) {
            // new customer
            NSString *currentTimeStamp = [self.saveParentInfoDictionary valueForKey:@"RequestTimeStamp"];
            _custID = [NSString createGuid];
            [self SaveParentInfo:currentTimeStamp];
        } else {
            // old customer
            NSString *currentTimeStamp = [COIDefaults GetCurrentTimeStamp];
            [self getParentID:currentTimeStamp];
        }
    }
}
-(void)SaveParentInfo:(NSString *)currentTimeStamp {
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self.saveParentInfoDictionary options:NSJSONWritingPrettyPrinted error:nil];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@svc/svcHifi.svc/SaveParentData",KWebServiceBaseURL]];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.timeoutIntervalForResource = kWebServiceTimeOutInterval;
    configuration.timeoutIntervalForRequest = kWebServiceTimeOutInterval;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWebServiceTimeOutInterval];
    // Specify that it will be a POST request
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSString *jsonstring =[[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSLog(@"json string is %@", jsonstring);
    request.HTTPBody = jsonData;
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
        if(data!=nil &&httpResponse.statusCode==200){
            
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSLog(@"save parent data response is %@",json);
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                if([[json valueForKey:@"ResponseCode"] isEqualToString:@"Y"]){
                    [self SaveChildInfo:[json valueForKey:@"RefNo"] withTimeStamp:currentTimeStamp];
                }else{
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    [COIDefaults ShowAlert:@"Error" andMessage:[json valueForKey:@"ProcessResponse"] withController:self];
                }
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                // Server Failure
            });
        }
    }];
    [postDataTask resume];
}
-(void)getParentID:(NSString *)currentTimeStamp {
    
    NSString *mallID = [COIDefaults getMallID].length == 0 ? @"" : [COIDefaults getMallID];
    NSString *userID = [COIDefaults getUserID].length == 0 ? @"" : [COIDefaults getUserID];
    
    NSString *hashParam  = [NSString stringWithFormat:@"CustID=%@&RequestTimeStamp=%@&AppKey=%@",_custID, currentTimeStamp, KAppKey];
    hashParam = [hashParam stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *checkSum = [COIDefaults hmacWithKey:KPrivateKey andData:hashParam];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            _custID,@"CustID",
                            mallID, @"RegMallID",
                            userID,@"UserID",
                            checkSum, @"TokenID",
                            currentTimeStamp, @"RequestTimeStamp",
                            KAppKey, @"AppKey",
                            nil];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@svc/svcHifi.svc/GetParentID",KWebServiceBaseURL]];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.timeoutIntervalForResource = kWebServiceTimeOutInterval;
    configuration.timeoutIntervalForRequest = kWebServiceTimeOutInterval;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWebServiceTimeOutInterval];
    // Specify that it will be a POST request
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSString *jsonstring =[[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSLog(@"json string is %@", jsonstring);
    request.HTTPBody = jsonData;
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if(data!=nil &&httpResponse.statusCode==200){
            
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSLog(@"get parent id response is %@",json);
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                if([[json valueForKey:@"ResponseCode"] isEqualToString:@"Y"]) {
                    [self SaveChildInfo:[json valueForKey:@"RefNo"] withTimeStamp:currentTimeStamp];
                }else{
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    [COIDefaults ShowAlert:@"Error" andMessage:[json valueForKey:@"ProcessResponse"] withController:self];
                }
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                // Server Failure
            });
        }
    }];
    [postDataTask resume];
}

-(void)SaveChildInfo:(NSString *)parentID withTimeStamp:(NSString *)currentTimeStamp
{
    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.Description contains[cd] \"%@\"",eventListTextField.text]];
    NSMutableArray *filteredContentArray = [[arrayOfEventList filteredArrayUsingPredicate:searchPredicate] mutableCopy];
    NSString *CampaignsApplied = [[filteredContentArray objectAtIndex:0]valueForKey:@"Code"];
    
    NSString *mallID = [COIDefaults getMallID].length == 0 ? @"" : [COIDefaults getMallID];
    NSString *userID = [COIDefaults getUserID].length == 0 ? @"" : [COIDefaults getUserID];
    
    NSString *hashParam  = [NSString stringWithFormat:@"ParentID=%@&RequestTimeStamp=%@&AppKey=%@",parentID, currentTimeStamp, KAppKey];
    hashParam = [hashParam stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *checkSum = [COIDefaults hmacWithKey:KPrivateKey andData:hashParam];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            parentID,@"ParentID",
                            [NSString createGuid], @"SessionID",
                            mallID, @"MallID",
                            userID,@"UserID",
                            arrChildrenName, @"ChildName",
                            CampaignsApplied, @"CampaignsApplied",
                            numberOfChildrenTextField.text,@"TotalChilds",
                            checkSum, @"TokenID",
                            currentTimeStamp, @"RequestTimeStamp",
                            KAppKey, @"AppKey",
                            nil];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@svc/svcHifi.svc/SaveChildInfo",KWebServiceBaseURL]];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.timeoutIntervalForResource = kWebServiceTimeOutInterval;
    configuration.timeoutIntervalForRequest = kWebServiceTimeOutInterval;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWebServiceTimeOutInterval];
    // Specify that it will be a POST request
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSString *jsonstring =[[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSLog(@"json string is %@", jsonstring);
    request.HTTPBody = jsonData;
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if(data!=nil &&httpResponse.statusCode==200){
            
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSLog(@"save child info response is %@",json);
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if([[json valueForKey:@"ResponseCode"] isEqualToString:@"Y"]) {
                    UIAlertAction *OkAction = [UIAlertAction
                                               actionWithTitle:@"OK"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                                               {
                                                   ThankYouViewController *thankuVC = [[ThankYouViewController alloc]initWithNibName:@"ThankYouViewController" bundle:nil];
                                                   [self.navigationController pushViewController:thankuVC animated:YES];
                                               }];
                    NSMutableArray *actionsArray = [[NSMutableArray alloc]initWithObjects:OkAction ,nil];
                    [COIDefaults ShowConfirmationAlert:@"Success" andMessage:[json valueForKey:@"ProcessResponse"] andActions:actionsArray withController:self];
                }else{
                    [COIDefaults ShowAlert:@"Error" andMessage:[json valueForKey:@"ProcessResponse"] withController:self];
                }
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                // Server Failure
            });
        }
    }];
    [postDataTask resume];
}
-(void)getEventList {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *currentTimeStamp = [COIDefaults GetCurrentTimeStamp];
    
    NSString *hashParam  = [NSString stringWithFormat:@"RequestTimeStamp=%@&AppKey=%@",currentTimeStamp,KAppKey];
    hashParam = [hashParam stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *checkSum = [COIDefaults hmacWithKey:KPrivateKey andData:hashParam];
    
    NSString *mallID = [COIDefaults getMallID].length == 0 ? @"" : [COIDefaults getMallID];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@svc/svcHifi.svc/GetEventList?RequestTimeStamp=%@&AppKey=%@&TokenID=%@&MallID=%@",KWebServiceBaseURL,[COIDefaults getUTF8String:currentTimeStamp],[COIDefaults getUTF8String:KAppKey],[COIDefaults getUTF8String:checkSum],[COIDefaults getUTF8String:mallID]]];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.timeoutIntervalForResource = kWebServiceTimeOutInterval;
    configuration.timeoutIntervalForRequest = kWebServiceTimeOutInterval;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:kWebServiceTimeOutInterval];
    
    // Specify that it will be a POST request
    request.HTTPMethod = @"GET";
    

    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if(data!=nil &&httpResponse.statusCode==200) {
    
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSLog(@"get event list response is %@",json);
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {

                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if([[json valueForKey:@"ResponseCode"] isEqualToString:@"Y"]) {
                    self->arrayOfEventList = [[json valueForKey:@"ListTitleContract"] mutableCopy];
                    
                    if (self->arrayOfEventList.count > 0) {
                        [self->arrayOfEventList removeObjectAtIndex:0];
                    }
                    if (self->arrayOfEventList.count > 0) {
                        
                        // remove --Select-- object from list
                        COIDefaultSearchViewController *defaultSearchVC = [[COIDefaultSearchViewController alloc]init];
                        defaultSearchVC.searchDelegate = self;
                        defaultSearchVC.contentArray = self->arrayOfEventList;
                        defaultSearchVC.objectType = @"Event List";
                        defaultSearchVC.titleString = @"Select Event List";
                        
                        UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:defaultSearchVC];
                        baNavController.preferredContentSize=CGSizeMake(376, 400);
                        baNavController.modalPresentationStyle = UIModalPresentationPopover;
                        UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
                        presentationController.delegate=self;
                        presentationController.sourceView = self->eventListTextField;
                        presentationController.sourceRect = CGRectMake(0, 0, self->eventListTextField.frame.size.width, self->eventListTextField.frame.size.height);
                        presentationController.permittedArrowDirections = UIPopoverArrowDirectionAny;
                        
                        [self presentViewController:baNavController animated:YES completion:^{
                            presentationController.passthroughViews=nil;
                            self.modalInPopover=YES;
                        }];
                    }else {
                        [COIDefaults ShowAlert:@"No Data" andMessage:@"Event list not found." withController:self];
                    }
                }else{
                    [COIDefaults ShowAlert:@"Error" andMessage:[json valueForKey:@"ProcessResponse"] withController:self];
                }
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
        }
    }];
    
    [postDataTask resume];
}

#pragma mark dropdown view delegate method
-(void)selectedObject:(id)selectedItem
{
    eventListTextField.text = [selectedItem valueForKey:@"Description"];
}

- (IBAction)addButtonTapped:(id)sender {
    [self.view endEditing:YES];

    if (childrenNameTextField.text.length == 0) {
        [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please enter child name to add" withController:self];
    } else {
        [arrChildrenName addObject:childrenNameTextField.text];
        childrenNameTextField.text = @"";
        
        [childrenNameTableView reloadData];
    }
    if (numberOfChildrenTextField.text.intValue == arrChildrenName.count) {
        btnAddChild.userInteractionEnabled = NO;
        btnAddChild.alpha = 0.5;
        
        childrenNameTextField.userInteractionEnabled = NO;
        childrenNameTextField.alpha = 0.5;
    } else {
        btnAddChild.userInteractionEnabled = YES;
        btnAddChild.alpha = 1.0;
        
        childrenNameTextField.userInteractionEnabled = YES;
        childrenNameTextField.alpha = 1.0;
    }
}

#pragma mark table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrChildrenName.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *identifier = @"cellIdentifier";
    ChildrenTableViewCell *cell = [childrenNameTableView dequeueReusableCellWithIdentifier:identifier];
    
    cell.layer.borderColor = TextFieldBorderColor.CGColor;
    cell.layer.masksToBounds = NO;
    cell.layer.borderWidth = 1;
    
    cell.lblName.text = [arrChildrenName objectAtIndex:indexPath.row];
    cell.lblTitle.text = [NSString stringWithFormat:@"Child Name %ld :",indexPath.row + 1];

    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //remove the deleted object from your data source.
        //If your data source is an NSMutableArray, do this
        [arrChildrenName removeObjectAtIndex:indexPath.row];
        [tableView reloadData]; // tell table to refresh now
        
        btnAddChild.userInteractionEnabled = YES;
        btnAddChild.alpha = 1.0;
        
        childrenNameTextField.userInteractionEnabled = YES;
        childrenNameTextField.alpha = 1.0;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
