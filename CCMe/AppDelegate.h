//
//  AppDelegate.h
//  CCMe
//
//  Created by appledev on 18/02/19.
//  Copyright © 2019 appledev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

