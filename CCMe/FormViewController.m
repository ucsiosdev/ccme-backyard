//
//  FormViewController.m
//  CityOfImagination
//
//  Created by Neha Gupta on 4/6/18.
//  Copyright © 2018 USHYAKU-IOS. All rights reserved.
//

#import "FormViewController.h"
#import "IQKeyboardManager.h"
#import "COIDefaultSearchViewController.h"
#import "COINetworkManager.h"
#import "COIDefaults.h"
#import "MBProgressHUD.h"
#import "ChildrenFormViewController.h"

@interface FormViewController ()

@end

@implementation FormViewController


#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define IS_IPAD_PRO_1366 (IS_IPAD && MAX(SCREEN_WIDTH,SCREEN_HEIGHT) == 1366.0)
#define IS_IPAD_PRO_1024 (IS_IPAD && MAX(SCREEN_WIDTH,SCREEN_HEIGHT) == 1024.0)


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    titleDicsArray = [[NSMutableArray alloc]initWithObjects:@"Mr.", @"Mrs.", @"Ms.", nil];
    residentialArray = [[NSMutableArray alloc]initWithObjects:@"Resident", @"Visitor", nil];
    nationalitiesDataArray = [COIDefaults getNationalityArray];
    if (nationalitiesDataArray.count == 0) {
        [self getNationalitiesData];
    }
    
    if (IS_IPAD_PRO_1366)
    {
        //its ipad Pro 12.9 inch screen
        
        xFormViewTopConstraint.constant = 150;
    } else {
        xFormViewTopConstraint.constant = 80;
    }
}

#pragma mark UITextFieldDelegateMethods
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    IQKeyboardManager *keyboardManager = [IQKeyboardManager sharedManager];
    if (keyboardManager.canGoNext) {
        [keyboardManager goNext];
    } else {
        [keyboardManager resignFirstResponder];
    }
    return NO;
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;   // return NO to not change text
{
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    if([string length] == 0)
    {
        if([textField.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if(textField.text.length==0 && [string isEqualToString:@" "])
    {
        return NO;
    }
    if( textField == txtMobileNumber &&  textField.text.length == 0 && string.length!=0){
        if([[string substringToIndex:1] isEqualToString:@"0"]){
            return NO;
        }
    }
    if(textField == txtMobileNumber)
    {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSString *expression = @"^[0-9]*$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return ((numberOfMatches != 0) && newLength<=10);
    }else if(textField == txtFirstName || textField == txtLastName)
    {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSString *expression = @"^[A-Za-z ]*$";
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        return ((numberOfMatches != 0) && newLength<=50);
    }
    else if(newLength>50)
    {
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if(textField == txtTitle && txtTitle.textState== Error){
        [txtTitle setTextState:normal];
    }else if(textField == txtFirstName && txtFirstName.textState== Error){
        [txtFirstName setTextState:normal];
    }else if(textField == txtLastName && txtLastName.textState== Error){
        [txtLastName setTextState:normal];
    }else if(textField == txtResidentialStatus && txtResidentialStatus.textState== Error){
        [txtResidentialStatus setTextState: normal];
    }else if(textField == txtNationality && txtNationality.textState== Error){
        [txtNationality setTextState: normal];
    }else if(textField == txtCountryOfResidence && txtCountryOfResidence.textState== Error  ){
        [txtCountryOfResidence setTextState: normal];
    }else if(textField == txtCity && txtCity.textState== Error  ){
        [txtCity setTextState: normal];
    }else if(textField == txtAreaOfResidence && txtAreaOfResidence.textState== Error  ){
        [txtAreaOfResidence setTextState: normal];
    }else if(textField == txtCountryCode && txtCountryCode.textState== Error){
        [txtCountryCode setTextState: normal];
    }else if(textField == txtMobileNumber && txtMobileNumber.textState== Error){
        [txtMobileNumber setTextState: normal];
    }else if(textField == txtEmailAddress && txtEmailAddress.textState== Error){
        [txtEmailAddress setTextState: normal];
    }
    
    if (textField == txtTitle) {
        [self resignKeyboard];
        
        objectType = @"Title";
        [self showDropDownList:titleDicsArray withTitle:@"Select Title" inView:textField];
        return NO;
    } else if (textField == txtResidentialStatus) {
        [self resignKeyboard];
        
        objectType = @"ResidentialStatus";
        [self showDropDownList:residentialArray withTitle:@"Select Residential Status" inView:textField];
        return NO;
    } else if (textField == txtNationality) {
        [self resignKeyboard];
        
        objectType = @"Nationality";
        [self showDropDownList:nationalitiesDataArray withTitle:@"Select Nationality" inView:textField];
        return NO;
    } else if (textField == txtCountryOfResidence) {
        [self resignKeyboard];
        
        objectType = @"CountryOfResidence";
        [self showDropDownList:nationalitiesDataArray withTitle:@"Select Residential Country" inView:textField];
        return NO;
    } else if (textField == txtCity) {
        [self resignKeyboard];
        
        if (txtCountryOfResidence.text.length == 0) {
            [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please select Residential Country first" withController:self];
        } else {
            objectType = @"City/Emirate";
            [self getResidenceCity];
        }
        return NO;
    } else if (textField == txtAreaOfResidence) {
        [self resignKeyboard];
        
        if (txtCity.text.length == 0) {
            [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please select City/Emirate first" withController:self];
        } else {
            objectType = @"AreaOfResidence";
            [self getResidenceArea];
        }
        return NO;
    } else if (textField == txtCountryCode) {
        [self resignKeyboard];
        
        objectType = @"CountryCode";
        [self showDropDownList:nationalitiesDataArray withTitle:@"Select Country Code" inView:textField];
        return NO;
    }
    return YES;
}

-(void)resignKeyboard {
    if ([txtFirstName isFirstResponder]) {
        [txtFirstName resignFirstResponder];
    }
    if ([txtLastName isFirstResponder]) {
        [txtLastName resignFirstResponder];
    }
    if ([txtMobileNumber isFirstResponder]) {
        [txtMobileNumber resignFirstResponder];
    }
    if ([txtEmailAddress isFirstResponder]) {
        [txtEmailAddress resignFirstResponder];
    }
}

-(void)showDropDownList:(NSMutableArray *)dropDownArray withTitle:(NSString *)titleString inView:(UITextField*)textfield {
    if (dropDownArray.count>0) {
        
        COIDefaultSearchViewController *defaultSearchVC = [[COIDefaultSearchViewController alloc]init];
        defaultSearchVC.searchDelegate = self;
        defaultSearchVC.contentArray = dropDownArray;
        defaultSearchVC.objectType = objectType;
        defaultSearchVC.titleString = titleString;
        
        UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:defaultSearchVC];
        baNavController.preferredContentSize=CGSizeMake(376, 400);
        baNavController.modalPresentationStyle = UIModalPresentationPopover;
        UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
        presentationController.delegate=self;
        presentationController.sourceView = textfield;
        presentationController.sourceRect = CGRectMake(0, 0, textfield.frame.size.width, textfield.frame.size.height);
        presentationController.permittedArrowDirections = UIPopoverArrowDirectionAny;
        
        [self presentViewController:baNavController animated:YES completion:^{
            presentationController.passthroughViews=nil;
            self.modalInPopover=YES;
            
        }];
    }
}

#pragma mark dropdown view delegate method
-(void)selectedObject:(id)selectedItem
{
    if ([objectType isEqualToString:@"Title"]) {
        
        txtTitle.text = selectedItem;
    } else if ([objectType isEqualToString:@"ResidentialStatus"]){
        
        txtResidentialStatus.text = selectedItem;
    } else if ([objectType isEqualToString:@"Nationality"]){
        
        txtNationality.text = [selectedItem valueForKey:@"Description"];
    } else if ([objectType isEqualToString:@"CountryOfResidence"]){
        
        txtCountryOfResidence.text = [selectedItem valueForKey:@"Description"];
        txtCity.text = @"";
        txtAreaOfResidence.text = @"";
    } else if ([objectType isEqualToString:@"City/Emirate"]){
        
        txtCity.text = [selectedItem valueForKey:@"CityName"];
        txtAreaOfResidence.text = @"";
    } else if ([objectType isEqualToString:@"AreaOfResidence"]){
        
        txtAreaOfResidence.text = [selectedItem valueForKey:@"AreaName"];
    } else if ([objectType isEqualToString:@"CountryCode"]){
        
        txtCountryCode.text = [selectedItem valueForKey:@"DialCode"];
        txtMobileNumber.text = @"";
    }
}

- (IBAction)submitButtonTapped:(id)sender {
    [self.view endEditing:YES];
    
    if (![[COINetworkManager retrieveManager] isNetworkReachable]) {
        [COIDefaults ShowAlert:@"No network" andMessage:@"Please check your network connection" withController:self];
    }
    else if(txtTitle.text.trimString.length==0 || txtTitle.textState == Error){
        [txtTitle setTextState:Error];
        [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please select title" withController:self];
    }
    else if(txtFirstName.text.trimString.length==0 || txtFirstName.textState == Error){
        [txtFirstName setTextState:Error];
        [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please enter first name" withController:self];
    }
    else if(txtLastName.text.trimString.length==0 || txtLastName.textState == Error){
        [txtLastName setTextState:Error];
        [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please enter last name" withController:self];
    }
    else if(txtResidentialStatus.text.length==0 || txtResidentialStatus.textState == Error)
    {
        [txtResidentialStatus setTextState:Error];
        [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please select your residential status" withController:self];
    }
    else if(txtNationality.text.length==0 || txtNationality.textState == Error)
    {
        [txtNationality setTextState:Error];
        [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please select your nationality" withController:self];
    }
    else if(txtCountryOfResidence.text.length==0 || txtCountryOfResidence.textState == Error)
    {
        [txtCountryOfResidence setTextState:Error];
        [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please select your residential country" withController:self];
    }
    else if(txtCity.text.length==0 || txtCity.textState == Error)
    {
        [txtCity setTextState:Error];
        [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please select your city/emirate" withController:self];
    }
    else if(txtAreaOfResidence.text.length==0 || txtAreaOfResidence.textState == Error)
    {
        [txtAreaOfResidence setTextState:Error];
        [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please select your area of residence" withController:self];
    }
    else if(txtCountryCode.text.length==0 || txtCountryCode.textState == Error)
    {
        [txtCountryCode setTextState:Error];
        [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please select country code" withController:self];
    }
    else if(txtMobileNumber.text.length==0 || txtMobileNumber.textState == Error){
        [txtMobileNumber setTextState:Error];
        [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please enter mobile number" withController:self];
    }
    else if(txtEmailAddress.text.length > 0 && (![COIDefaults validateEmail:txtEmailAddress.text] || txtEmailAddress.textState == Error)){
            [txtEmailAddress setTextState:Error];
            [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please enter valid email address" withController:self];
    }
    else{
        NSString *titleCode, *residenceStatus;
        
        if ([txtTitle.text isEqualToString:@"Mr."]) {
            titleCode = @"MR";
        } else if ([txtTitle.text isEqualToString:@"Mrs."]){
            titleCode = @"MRS";
        } else {
            titleCode = @"MS";
        }
        
        NSString *nationalityCode, *residenceCountryCode, *cityCode, *areaCode;

        NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.Description contains[cd] '%@'",txtNationality.text]];
        NSMutableArray *filteredContentArray = [[nationalitiesDataArray filteredArrayUsingPredicate:searchPredicate] mutableCopy];
        if (filteredContentArray.count > 0) {
            nationalityCode = [[filteredContentArray objectAtIndex:0]valueForKey:@"Code"];
        }
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.Description contains[cd] '%@'",txtCountryOfResidence.text]];
        NSMutableArray *filteredArray = [[nationalitiesDataArray filteredArrayUsingPredicate:predicate] mutableCopy];
        if (filteredArray.count > 0) {
            residenceCountryCode = [[filteredArray objectAtIndex:0]valueForKey:@"Code"];
        }
        
        NSPredicate *cityPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.CityName contains[cd] '%@'",txtCity.text]];
        NSMutableArray *cityFilteredArray = [[residentialCityArray filteredArrayUsingPredicate:cityPredicate] mutableCopy];
        if (cityFilteredArray.count > 0) {
            cityCode = [[cityFilteredArray objectAtIndex:0]valueForKey:@"CityID"];
        }
        
        NSPredicate *areaPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.AreaName contains[cd] '%@'",txtAreaOfResidence.text]];
        NSMutableArray *areaFilteredArray = [[residentialAreaArray filteredArrayUsingPredicate:areaPredicate] mutableCopy];
        if (areaFilteredArray.count > 0) {
            areaCode = [[areaFilteredArray objectAtIndex:0]valueForKey:@"AreaID"];
        }

        
        if ([txtResidentialStatus.text isEqualToString:@"Resident"]) {
            residenceStatus = @"R";
        } else if ([txtResidentialStatus.text isEqualToString:@"Visitor"]){
            residenceStatus = @"V";
        }
        
        NSString *currentTimeStamp = [COIDefaults GetCurrentTimeStamp];
        NSMutableArray *signUpInputKeys = [[NSMutableArray alloc] initWithObjects:
                                           @"CustRefNo",
                                           @"Title",
                                           @"FirstName",
                                           @"LastName",
                                           @"CountryCode",
                                           @"AreaCode",
                                           @"MobileNo",
                                           @"Email",
                                           @"RequestTimeStamp",
                                           @"AppKey",
                                           nil];
        
        NSMutableDictionary *signUpRequestParametersDic=[[NSMutableDictionary alloc]initWithObjectsAndKeys:
                                                         [NSString createGuid],@"CustRefNo",
                                                         titleCode,@"Title",
                                                         txtFirstName.text,@"FirstName",
                                                         txtLastName.text,@"LastName",
                                                         txtCountryCode.text,@"CountryCode",
                                                         @"",@"AreaCode",
                                                         txtMobileNumber.text,@"MobileNo",
                                                         txtEmailAddress.text,@"Email",
                                                         currentTimeStamp,@"RequestTimeStamp",
                                                         KAppKey,@"AppKey",
                                                         nil];
        NSString *hashParam=@"";
        for (NSInteger i=0; i<signUpInputKeys.count; i++) {
            hashParam = [hashParam stringByAppendingString:[signUpInputKeys objectAtIndex:i]];
            hashParam = [hashParam stringByAppendingString:@"="];
            hashParam = [hashParam stringByAppendingString:[signUpRequestParametersDic valueForKey:[signUpInputKeys objectAtIndex:i]]];
            if(i<signUpInputKeys.count-1){
                hashParam = [hashParam stringByAppendingString:@"&"];
            }
        }

        hashParam = [hashParam stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString *result =[COIDefaults hmacWithKey:KPrivateKey andData:hashParam];
        
        [signUpRequestParametersDic setValue:result forKey:@"TokenID"];
        [signUpRequestParametersDic setValue:[COIDefaults getMallID].length == 0 ? @"" : [COIDefaults getMallID] forKey:@"RegMallID"];
        [signUpRequestParametersDic setValue:[COIDefaults getUserID].length == 0 ? @"" : [COIDefaults getUserID] forKey:@"UserID"];
        [signUpRequestParametersDic setValue:nationalityCode forKey:@"Nationality"];
        [signUpRequestParametersDic setValue:residenceCountryCode forKey:@"CountryResidence"];
        
        [signUpRequestParametersDic setValue:residenceStatus forKey:@"ResidenceStatus"];
        [signUpRequestParametersDic setValue:cityCode forKey:@"ResidenceCity"];
        [signUpRequestParametersDic setValue:areaCode forKey:@"ResidenceArea"];
        [signUpRequestParametersDic setValue:@"Y" forKey:@"MarketingEmail"];
        [signUpRequestParametersDic setValue:@"Y" forKey:@"MarketingSMS"];
        [signUpRequestParametersDic setValue:@"Y" forKey:@"PrivacyPolicy"];

        ChildrenFormViewController *childFormVC = [[ChildrenFormViewController alloc]initWithNibName:@"ChildrenFormViewController" bundle:nil];
        childFormVC.saveParentInfoDictionary = signUpRequestParametersDic;
        [self.navigationController pushViewController:childFormVC animated:YES];
    }
}

-(void)getNationalitiesData {
    
    NSString *currentTimeStamp = [COIDefaults GetCurrentTimeStamp];
    
    NSString *hashParam  =[NSString stringWithFormat:@"RequestTimeStamp=%@&AppKey=%@",currentTimeStamp, KAppKey];
    hashParam = [hashParam stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *result =[COIDefaults hmacWithKey:KPrivateKey andData:hashParam];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@svc/svcHifi.svc/GetNationality?RequestTimeStamp=%@&AppKey=%@&TokenID=%@",KWebServiceBaseURL, [COIDefaults getUTF8String:currentTimeStamp], [COIDefaults getUTF8String:KAppKey], [COIDefaults getUTF8String:result]]];
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.timeoutIntervalForResource = kWebServiceTimeOutInterval;
    configuration.timeoutIntervalForRequest = kWebServiceTimeOutInterval;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWebServiceTimeOutInterval];
    // Specify that it will be a GET request
    request.HTTPMethod = @"GET";
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if(data!=nil &&httpResponse.statusCode==200){
            
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSLog(@"Nationality Data is %@",json);
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if([[json valueForKey:@"ResponseCode"] isEqualToString:@"Y"]){
                    
                    self->nationalitiesDataArray = [[json valueForKey:@"ListNationalityContract"] mutableCopy];
                    if (self->nationalitiesDataArray.count > 0) {
                        // remove --Select-- object from list
                        [self->nationalitiesDataArray removeObjectAtIndex:0];
                    }

                    if (self->nationalitiesDataArray.count>0) {
                        [COIDefaults setNationalityArray:self->nationalitiesDataArray];
                    } else {
                        [COIDefaults ShowAlert:@"No Data" andMessage:@"Nationality Data not found." withController:self];
                    }
                }else{
                    [COIDefaults ShowAlert:@"Error" andMessage:[json valueForKey:@"ProcessResponse"] withController:self];
                }
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                // Server Failure
            });
        }
    }];
    [postDataTask resume];
}

-(void)getResidenceCity {
    
    NSString *currentTimeStamp = [COIDefaults GetCurrentTimeStamp];
    
    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.Description contains[cd] \"%@\"",txtCountryOfResidence.text]];
    NSMutableArray *filteredContentArray = [[nationalitiesDataArray filteredArrayUsingPredicate:searchPredicate] mutableCopy];
    NSString *countryID;
    if (filteredContentArray.count > 0) {
        countryID = [[filteredContentArray objectAtIndex:0]valueForKey:@"Code"];
    }
    
    NSString *hashParam  =[NSString stringWithFormat:@"Country=%@&RequestTimeStamp=%@&AppKey=%@",countryID, currentTimeStamp, KAppKey];
    hashParam = [hashParam stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *result =[COIDefaults hmacWithKey:KPrivateKey andData:hashParam];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@svc/svcHifi.svc/GetResidenceCity?Country=%@&RequestTimeStamp=%@&AppKey=%@&TokenID=%@",KWebServiceBaseURL, countryID, [COIDefaults getUTF8String:currentTimeStamp], [COIDefaults getUTF8String:KAppKey], [COIDefaults getUTF8String:result]]];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.timeoutIntervalForResource = kWebServiceTimeOutInterval;
    configuration.timeoutIntervalForRequest = kWebServiceTimeOutInterval;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWebServiceTimeOutInterval];
    // Specify that it will be a GET request
    request.HTTPMethod = @"GET";
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if(data!=nil &&httpResponse.statusCode==200){
            
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSLog(@"Residence City Data is %@",json);
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if([[json valueForKey:@"ResponseCode"] isEqualToString:@"Y"]){
                    
                    self->residentialCityArray = [[json valueForKey:@"ListCityContract"] mutableCopy];
                    if (self->residentialCityArray.count > 0) {
                        // remove --Select-- object from list
                        [self->residentialCityArray removeObjectAtIndex:0];
                    }
                    
                    if (self->residentialCityArray.count>0) {
                        [self showDropDownList:self->residentialCityArray withTitle:@"Select City/Emirate" inView:self->txtCity];
                    } else {
                        [COIDefaults ShowAlert:@"No Data" andMessage:@"City/Emirate Data not found." withController:self];
                    }
                    
                }else{
                    [COIDefaults ShowAlert:@"Error" andMessage:[json valueForKey:@"ProcessResponse"] withController:self];
                }
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                // Server Failure
            });
        }
    }];
    [postDataTask resume];
}

-(void)getResidenceArea {
    
    NSString *currentTimeStamp = [COIDefaults GetCurrentTimeStamp];
    
    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.CityName contains[cd] \"%@\"",txtCity.text]];
    NSMutableArray *filteredContentArray = [[residentialCityArray filteredArrayUsingPredicate:searchPredicate] mutableCopy];
    NSString *cityID = @"";
    if (filteredContentArray.count > 0) {
        cityID = [[filteredContentArray objectAtIndex:0]valueForKey:@"CityID"];
    }
    
    NSString *hashParam  =[NSString stringWithFormat:@"City=%@&RequestTimeStamp=%@&AppKey=%@",cityID, currentTimeStamp, KAppKey];
    hashParam = [hashParam stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *result =[COIDefaults hmacWithKey:KPrivateKey andData:hashParam];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@svc/svcHifi.svc/GetResidenceArea?City=%@&RequestTimeStamp=%@&AppKey=%@&TokenID=%@",KWebServiceBaseURL, cityID, [COIDefaults getUTF8String:currentTimeStamp], [COIDefaults getUTF8String:KAppKey], [COIDefaults getUTF8String:result]]];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.timeoutIntervalForResource = kWebServiceTimeOutInterval;
    configuration.timeoutIntervalForRequest = kWebServiceTimeOutInterval;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWebServiceTimeOutInterval];
    // Specify that it will be a GET request
    request.HTTPMethod = @"GET";
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if(data!=nil &&httpResponse.statusCode==200){
            
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSLog(@"Residence Area Data is %@",json);
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if([[json valueForKey:@"ResponseCode"] isEqualToString:@"Y"]){
                    
                    self->residentialAreaArray = [[json valueForKey:@"ListAreaContract"] mutableCopy];
                    if (self->residentialAreaArray.count > 0) {
                        // remove --Select-- object from list
                        [self->residentialAreaArray removeObjectAtIndex:0];
                    }
                    
                    if (self->residentialAreaArray.count>0) {
                        [self showDropDownList:self->residentialAreaArray withTitle:@"Select Area of Residence" inView:self->txtAreaOfResidence];
                    } else {
                        [COIDefaults ShowAlert:@"No Data" andMessage:@"Residence Area Data not found." withController:self];
                    }
                    
                }else{
                    [COIDefaults ShowAlert:@"Error" andMessage:[json valueForKey:@"ProcessResponse"] withController:self];
                }
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                // Server Failure
            });
        }
    }];
    [postDataTask resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
