//
//  main.m
//  CCMe
//
//  Created by appledev on 18/02/19.
//  Copyright © 2019 appledev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
