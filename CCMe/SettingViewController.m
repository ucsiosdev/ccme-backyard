//
//  SettingViewController.m
//  Customer Registration
//
//  Created by Neha Gupta on 11/14/18.
//  Copyright © 2018 USHYAKU-IOS. All rights reserved.
//

#import "SettingViewController.h"
#import "COIDefaultPopUpViewController.h"
#import "COIDefaultSearchViewController.h"
#import "COIDefaults.h"
#import "MBProgressHUD.h"
#import "COINetworkManager.h"
#import "FormViewController.h"
#import "MobileNumberSearchViewController.h"

@interface SettingViewController ()

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark UITextFieldDelegateMethods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if(textField == txtCSOAccount && txtCSOAccount.textState== Error){
        [txtCSOAccount setTextState:normal];
    }else if(textField == txtDefaultMall && txtDefaultMall.textState== Error){
        [txtDefaultMall setTextState:normal];
    }
    
    if (textField == txtDefaultMall) {
        [self resignKeyboard];
        
        if (arrayMallList.count == 0) {
            [self getMallList];
        } else {
            COIDefaultSearchViewController *defaultSearchVC = [[COIDefaultSearchViewController alloc]init];
            defaultSearchVC.searchDelegate = self;
            defaultSearchVC.contentArray = arrayMallList;
            defaultSearchVC.objectType = @"Mall List";
            defaultSearchVC.titleString = @"Select Mall List";
            
            UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:defaultSearchVC];
            baNavController.preferredContentSize=CGSizeMake(376, 400);
            baNavController.modalPresentationStyle = UIModalPresentationPopover;
            UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
            presentationController.delegate=self;
            presentationController.sourceView = txtDefaultMall;
            presentationController.sourceRect = CGRectMake(0, 0, txtDefaultMall.frame.size.width, txtDefaultMall.frame.size.height);
            presentationController.permittedArrowDirections = UIPopoverArrowDirectionAny;
            
            [self presentViewController:baNavController animated:YES completion:^{
                presentationController.passthroughViews=nil;
                self.modalInPopover=YES;
            }];
        }
        return NO;
    }
    return YES;
}

-(void)resignKeyboard {
    if ([txtCSOAccount isFirstResponder]) {
        [txtCSOAccount resignFirstResponder];
    }
    if ([txtDefaultMall isFirstResponder]) {
        [txtDefaultMall resignFirstResponder];
    }
}

#pragma mark dropdown view delegate method
-(void)selectedObject:(id)selectedItem
{
    txtDefaultMall.text = [selectedItem valueForKey:@"LoginMallName"];
}

#pragma mark PopUp delegate method
-(void)setFieldInput:(NSString *)text
{
    txtCSOAccount.text = text;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)closeButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)submitButtonTapped:(id)sender {
    [self.view endEditing:YES];
    
    if (![[COINetworkManager retrieveManager] isNetworkReachable]) {
        [COIDefaults ShowAlert:@"No network" andMessage:@"Please check your network connection" withController:self];
    }
    else if(txtCSOAccount.text.trimString.length==0 || txtCSOAccount.textState == Error){
        [txtCSOAccount setTextState:Error];
        [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please enter CSO Account." withController:self];
    }
    else if(txtDefaultMall.text.trimString.length==0 || txtDefaultMall.textState == Error){
        [txtDefaultMall setTextState:Error];
        [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please select Default Mall." withController:self];
    }else {

        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.LoginMallName contains[cd] \"%@\"",txtDefaultMall.text]];
        NSMutableArray *filteredContentArray = [[arrayMallList filteredArrayUsingPredicate:searchPredicate] mutableCopy];
        NSString *mallID = [[filteredContentArray objectAtIndex:0]valueForKey:@"LoginMallID"];
        
        NSString *currentTimeStamp = [COIDefaults GetCurrentTimeStamp];
        
        NSString *hashParam  = [NSString stringWithFormat:@"ApprovalCode=%@&RequestTimeStamp=%@&AppKey=%@",txtCSOAccount.text, currentTimeStamp, KAppKey];
        hashParam = [hashParam stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString *checkSum = [COIDefaults hmacWithKey:KPrivateKey andData:hashParam];
        
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                txtCSOAccount.text, @"ApprovalCode",
                                mallID, @"MallID",
                                checkSum, @"TokenID",
                                currentTimeStamp, @"RequestTimeStamp",
                                KAppKey, @"AppKey",
                                nil];
        
        NSLog(@"approval code Param %@",params);
        
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@svc/svcHifi.svc/CheckApprovalCode",KWebServiceBaseURL]];
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        configuration.timeoutIntervalForResource = kWebServiceTimeOutInterval;
        configuration.timeoutIntervalForRequest = kWebServiceTimeOutInterval;
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:kWebServiceTimeOutInterval];
        // Specify that it will be a POST request
        request.HTTPMethod = @"POST";
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        NSString *jsonstring =[[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"json string is %@", jsonstring);
        request.HTTPBody = jsonData;
        
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
            if(data!=nil &&httpResponse.statusCode==200){
                
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                NSLog(@"save customer response is %@",json);
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    if([[json valueForKey:@"ResponseCode"] isEqualToString:@"Y"]){
                        [COIDefaults setMallID:mallID];
                        [COIDefaults setApprovalCode:self->txtCSOAccount.text];
                        [self validateApprovalCode];
                    }else{
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        [COIDefaults ShowAlert:@"Error" andMessage:[json valueForKey:@"ProcessResponse"] withController:self];
                    }
                });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                });
            }
        }];
        [postDataTask resume];
    }
}

- (void)validateApprovalCode{
    
    NSString *currentTimeStamp = [COIDefaults GetCurrentTimeStamp];
    NSString *hashParam  = [NSString stringWithFormat:@"ApprovalCode=%@&RequestTimeStamp=%@&AppKey=%@",txtCSOAccount.text, currentTimeStamp, KAppKey];
    hashParam = [hashParam stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *checkSum = [COIDefaults hmacWithKey:KPrivateKey andData:hashParam];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            txtCSOAccount.text, @"ApprovalCode",
                            checkSum, @"TokenID",
                            currentTimeStamp, @"RequestTimeStamp",
                            KAppKey, @"AppKey",
                            nil];
    
    NSLog(@"approval code Param %@",params);
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@svc/svcHifi.svc/ValidateApprovalCode",KWebServiceBaseURL]];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.timeoutIntervalForResource = kWebServiceTimeOutInterval;
    configuration.timeoutIntervalForRequest = kWebServiceTimeOutInterval;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWebServiceTimeOutInterval];
    // Specify that it will be a POST request
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSString *jsonstring =[[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSLog(@"json string is %@", jsonstring);
    request.HTTPBody = jsonData;
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
        if(data!=nil &&httpResponse.statusCode==200){
            
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSLog(@"save customer response is %@",json);
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if([[json valueForKey:@"ResponseCode"] isEqualToString:@"Y"]){
                    
                    [COIDefaults setUserID:[json valueForKey:@"UserID"]];
                    UIAlertAction *OkAction = [UIAlertAction
                                               actionWithTitle:@"OK"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                                               {
                                                   MobileNumberSearchViewController *formVC = [[MobileNumberSearchViewController alloc]initWithNibName:@"MobileNumberSearchViewController" bundle:nil];
                                                   [self.navigationController pushViewController:formVC animated:YES];
                                                   
                                               }];
                    NSMutableArray *actionsArray = [[NSMutableArray alloc]initWithObjects:OkAction ,nil];
                    [COIDefaults ShowConfirmationAlert:@"Success" andMessage:[json valueForKey:@"ProcessResponse"] andActions:actionsArray withController:self];
                    
                }else{
                    [COIDefaults ShowAlert:@"Error" andMessage:[json valueForKey:@"ProcessResponse"] withController:self];
                }
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                // Server Failure
            });
        }
    }];
    [postDataTask resume];

}

- (IBAction)resetButtonTapped:(id)sender {
    txtDefaultMall.text = @"";
    txtCSOAccount.text = @"";
}

-(void)getMallList {
    
    NSString *currentTimeStamp = [COIDefaults GetCurrentTimeStamp];
    
    NSString *hashParam  =[NSString stringWithFormat:@"RequestTimeStamp=%@&AppKey=%@",currentTimeStamp, KAppKey];
    hashParam = [hashParam stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *result =[COIDefaults hmacWithKey:KPrivateKey andData:hashParam];
  
   
    NSLog(@"URLSTR  %@ ",[NSString stringWithFormat:@"%@svc/svcHifi.svc/GetMallsList?RequestTimeStamp=%@&AppKey=%@&TokenID=%@",KWebServiceBaseURL, [COIDefaults getUTF8String:currentTimeStamp], [COIDefaults getUTF8String:KAppKey], [COIDefaults getUTF8String:result]]);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@svc/svcHifi.svc/GetMallsList?RequestTimeStamp=%@&AppKey=%@&TokenID=%@",KWebServiceBaseURL, [COIDefaults getUTF8String:currentTimeStamp], [COIDefaults getUTF8String:KAppKey], [COIDefaults getUTF8String:result]]];
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.timeoutIntervalForResource = kWebServiceTimeOutInterval;
    configuration.timeoutIntervalForRequest = kWebServiceTimeOutInterval;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:kWebServiceTimeOutInterval];
    // Specify that it will be a GET request
    request.HTTPMethod = @"GET";
    
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
        if(data!=nil &&httpResponse.statusCode==200){
            
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSLog(@"mall list is %@",json);
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if([[json valueForKey:@"Result"] isEqualToString:@"Success"]){
                    
                    self->arrayMallList = [[json valueForKey:@"ListLoginMall"] mutableCopy];
                    
                    if (self->arrayMallList.count>0) {
                        
                        // remove --Select-- object from list
                        [self->arrayMallList removeObjectAtIndex:0];
                        
                        COIDefaultSearchViewController *defaultSearchVC = [[COIDefaultSearchViewController alloc]init];
                        defaultSearchVC.searchDelegate = self;
                        defaultSearchVC.contentArray = self->arrayMallList;
                        defaultSearchVC.objectType = @"Mall List";
                        defaultSearchVC.titleString = @"Select Mall List";
                        
                        UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:defaultSearchVC];
                        baNavController.preferredContentSize=CGSizeMake(376, 400);
                        baNavController.modalPresentationStyle = UIModalPresentationPopover;
                        UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
                        presentationController.delegate=self;
                        presentationController.sourceView = self->txtDefaultMall;
                        presentationController.sourceRect = CGRectMake(0, 0, self->txtDefaultMall.frame.size.width, self->txtDefaultMall.frame.size.height);
                        presentationController.permittedArrowDirections = UIPopoverArrowDirectionAny;
                        
                        [self presentViewController:baNavController animated:YES completion:^{
                            presentationController.passthroughViews=nil;
                            self.modalInPopover=YES;
                        }];
                    } else {
                        [COIDefaults ShowAlert:@"No Data" andMessage:@"Mall list not found." withController:self];
                    }
                    
                }else{
                    [COIDefaults ShowAlert:@"Error" andMessage:[json valueForKey:@"ProcessResponse"] withController:self];
                }
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                // Server Failure
            });
        }
    }];
    [postDataTask resume];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
