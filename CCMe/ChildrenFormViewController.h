//
//  ChildrenFormViewController.h
//  CCMe
//
//  Created by appledev on 19/02/19.
//  Copyright © 2019 appledev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "COITextField.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChildrenFormViewController : UIViewController{
    
    IBOutlet COITextField *eventListTextField;
    IBOutlet COITextField *childrenNameTextField;
    IBOutlet COITextField *numberOfChildrenTextField;
    
    NSMutableArray *arrayOfEventList;
    NSMutableArray *arrChildrenName;

    IBOutlet NSLayoutConstraint *xFormViewTopConstraint;
    IBOutlet UITableView *childrenNameTableView;
    
    IBOutlet UIButton *btnAddChild;
}
@property(strong,nonatomic) NSString *custID;
@property(strong,nonatomic) NSMutableDictionary *saveParentInfoDictionary;

@end

NS_ASSUME_NONNULL_END
