//
//  COIDefaultSearchTableViewCell.m
//  CityOfImagination
//
//  Created by Neha Gupta on 4/6/18.
//  Copyright © 2018 USHYAKU-IOS. All rights reserved.
//

#import "COIDefaultSearchTableViewCell.h"

@implementation COIDefaultSearchTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
