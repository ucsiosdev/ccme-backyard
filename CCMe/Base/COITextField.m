//
//  COITextField.m
//  CityOfImagination
//
//  Created by Neha Gupta on 4/6/18.
//  Copyright © 2018 USHYAKU-IOS. All rights reserved.
//

#import "COITextField.h"

@implementation COITextField
@synthesize dropdownImageView, ErrorString, textState;

-(void)awakeFromNib
{
    [super awakeFromNib];

    self.autocorrectionType = UITextAutocorrectionTypeNo;
    self.font = kSB_FONT_Medium(20);
    self.textColor = TextFieldDescriptionLabelColor;
    self.tintColor = TextFieldDescriptionLabelColor;
    self.backgroundColor = TextFieldBackgroundColor;
    
    self.layer.borderColor = TextFieldBorderColor.CGColor;
    self.layer.masksToBounds = NO;
    self.layer.cornerRadius = 21.0;
    self.layer.borderWidth = 1.5;
    
    UIView *leftPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.height*0.2, self.frame.size.height)];
    self.leftView = leftPaddingView;
    leftPaddingView.userInteractionEnabled=NO;
    self.leftViewMode = UITextFieldViewModeAlways;
}

-(void)setDropDownImage:(NSString *)ImageName
{
    if(ImageName.length>0){
        UIView *dropDownView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 15)];/* 22 is padding +imageView Width*/
        dropdownImageView=[[UIImageView alloc]initWithFrame:CGRectMake(5,0,20,15)];
        [dropdownImageView setImage:[UIImage imageNamed:ImageName/*@"Arrow_DropDown"*/]];
        [dropDownView addSubview:dropdownImageView];
        self.rightView = dropDownView;
        dropDownView.userInteractionEnabled=NO;
        self.rightViewMode = UITextFieldViewModeAlways;
    }
    else
    {
        
    }
}
-(void)setTextState:(TextfieldTextState)txtState{
    if(txtState == normal){
        self.textColor = TextFieldDescriptionLabelColor;
//        self.background = [UIImage imageNamed:@"WhiteTextFieldBg"];
    }else{
        self.textColor = [UIColor redColor];
//        self.background = [UIImage imageNamed:@"TextfieldErrorBg"];
    }
    textState = txtState;
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
