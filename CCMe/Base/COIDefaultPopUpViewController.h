//
//  COIDefaultPopUpViewController.h
//  Customer Registration
//
//  Created by Neha Gupta on 11/14/18.
//  Copyright © 2018 USHYAKU-IOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "COITextField.h"

@protocol PopUpDelegate <NSObject>
-(void)setFieldInput:(NSString *)text;
@end

@interface COIDefaultPopUpViewController : UIViewController
{
    id popUpDelegate;
    
    IBOutlet UILabel *lblTitle;
    IBOutlet COITextField *txtField;
}
@property(strong,nonatomic) id titleString;
@property(nonatomic) id popUpDelegate;

@end

