//
//  COIDefaultSearchViewController.m
//  CityOfImagination
//
//  Created by Neha Gupta on 4/6/18.
//  Copyright © 2018 USHYAKU-IOS. All rights reserved.
//

#import "COIDefaultSearchViewController.h"
#import "COIDefaultSearchTableViewCell.h"

@interface COIDefaultSearchViewController ()

@end

@implementation COIDefaultSearchViewController
@synthesize searchTableView,contentArray,objectType,searchDelegate, titleString;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    titleLabel.text = titleString;
    unfilteredArray=contentArray;
    
    selectedCellIndexPathsArray=[[NSMutableArray alloc]init];
    selectedObjectsArrayforMultiSelect=[[NSMutableArray alloc]init];
    
    self.navigationController.navigationBarHidden=YES;

    if (!([objectType isEqualToString:@"Title"] || [objectType isEqualToString:@"ResidentialStatus"])) {
        searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
        searchController.hidesNavigationBarDuringPresentation = YES;
        [searchController setDimsBackgroundDuringPresentation:NO];
        
        // The searchcontroller's searchResultsUpdater property will contain our tableView.
        searchController.searchResultsUpdater = self;
        
        // The searchBar contained in XCode's storyboard is a leftover from UISearchDisplayController.
        // Don't use this. Instead, we'll create the searchBar programatically.
        searchController.searchBar.frame = CGRectMake(searchController.searchBar.frame.origin.x,
                                                      searchController.searchBar.frame.origin.y,
                                                      searchTableView.frame.size.width, 56.0);
        
        
        searchController.searchBar.delegate = self;
        
        UIView* headerContentView =[[UIView alloc] initWithFrame:CGRectMake(searchController.searchBar.frame.origin.x, searchController.searchBar.frame.origin.y, searchTableView.frame.size.width, 60)];
        [headerContentView addSubview:searchController.searchBar];
        searchTableView.tableHeaderView = headerContentView;
    }
}

#pragma mark UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (contentArray.count>0) {
        return contentArray.count;
    }
    else
    {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    return 44.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    COIDefaultSearchTableViewCell *cell = (COIDefaultSearchTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"DefaultSearchTableCell"];
    if (cell==nil) {
        NSArray * nibArray=[[NSBundle mainBundle] loadNibNamed:@"COIDefaultSearchTableViewCell" owner:self options:nil];
        cell=[nibArray objectAtIndex:0];
    }

    if ([objectType isEqualToString:@"Title"] || [objectType isEqualToString:@"ResidentialStatus"]) {
        cell.titleLbl.text = [contentArray objectAtIndex:indexPath.row];
    } else if ([objectType isEqualToString:@"CountryCode"]) {
        NSMutableDictionary *dict = [contentArray objectAtIndex:indexPath.row];
        cell.titleLbl.text = [[NSString stringWithFormat:@"+%@",[dict valueForKey:@"DialCode"]] stringByAppendingString:[NSString stringWithFormat:@" - %@",[dict valueForKey:@"Description"]]];
    }
    else if ([objectType isEqualToString:@"Mall List"]) {
        cell.titleLbl.text = [[contentArray objectAtIndex:indexPath.row] valueForKey:@"LoginMallName"];
    }
    else if ([objectType isEqualToString:@"Event List"]) {
        cell.titleLbl.text = [[contentArray objectAtIndex:indexPath.row] valueForKey:@"Description"];
    }
    else if ([objectType isEqualToString:@"City/Emirate"]) {
        cell.titleLbl.text = [[contentArray objectAtIndex:indexPath.row] valueForKey:@"CityName"];
    }
    else if ([objectType isEqualToString:@"AreaOfResidence"]) {
        cell.titleLbl.text = [[contentArray objectAtIndex:indexPath.row] valueForKey:@"AreaName"];
    }
    else
    {
        cell.titleLbl.text = [[contentArray objectAtIndex:indexPath.row] valueForKey:@"Description"];
    }
    
    return cell;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [searchController setActive:NO];
    [searchController.searchBar resignFirstResponder];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self.searchDelegate selectedObject:[self->contentArray objectAtIndex:indexPath.row]];
    });
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    // [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark search bar methods
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSString *searchString = searchBar.text;
    [self startSearchwithString:searchString];
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([NSString isEmpty:searchText]) {
        contentArray=unfilteredArray;
        [searchTableView reloadData];
    }
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    contentArray=unfilteredArray;
    [searchTableView reloadData];
}

-(void)startSearchwithString:(NSString*)searchString
{
    if ([NSString isEmpty:searchString]==NO) {
        
        NSPredicate *searchPredicate;
        
        if ([objectType isEqualToString:@"CountryCode"]) {
            NSPredicate *predicate1 = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.DialCode contains[cd] '%@'",searchString]];
            NSPredicate *predicate2 = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.Description contains[cd] '%@'",searchString]];
            searchPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate1, predicate2]];
        }
        else if ([objectType isEqualToString:@"Mall List"]) {
            searchPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.LoginMallName contains[cd] '%@'",searchString]];
        }
        else if ([objectType isEqualToString:@"Event List"]) {
            searchPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.Description contains[cd] '%@'",searchString]];
        }
        else if ([objectType isEqualToString:@"City/Emirate"]) {
            searchPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.CityName contains[cd] '%@'",searchString]];
        }
        else if ([objectType isEqualToString:@"AreaOfResidence"]) {
            searchPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.AreaName contains[cd] '%@'",searchString]];
        }
        else
        {
            searchPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF.Description contains[cd] '%@'",searchString]];
        }
        
        contentArray = [[unfilteredArray filteredArrayUsingPredicate:searchPredicate] mutableCopy];
        [searchTableView reloadData];
    }
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = searchController.searchBar.text;
    [self startSearchwithString:searchString];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
