//
//  COIDefaultPopUpViewController.m
//  Customer Registration
//
//  Created by Neha Gupta on 11/14/18.
//  Copyright © 2018 USHYAKU-IOS. All rights reserved.
//

#import "COIDefaultPopUpViewController.h"
#import "COIConstant.h"
#import "COIDefaults.h"
#import "IQKeyboardManager.h"
#import "MBProgressHUD.h"

@interface COIDefaultPopUpViewController ()

@end

@implementation COIDefaultPopUpViewController
@synthesize titleString, popUpDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    lblTitle.text = titleString;
    
    if ([titleString isEqualToString:@"Setting Password"]) {
        txtField.secureTextEntry = true;
    }
    
    self.navigationController.navigationBarHidden=YES;
}
- (IBAction)closeButtonTapped:(id)sender {
    [self.view endEditing:YES];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveButtonTapped:(id)sender {
    [self.view endEditing:YES];

    if(txtField.text.length == 0 || txtField.textState == Error){
        [txtField setTextState:Error];
        [COIDefaults ShowAlert:@"Missing Data" andMessage:[NSString stringWithFormat:@"Please enter %@.",titleString] withController:self];
    } else {
        if([titleString isEqualToString:@"Setting Password"])
        {
            if ([txtField.text isEqualToString:@"@b@cu$"]) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [self.popUpDelegate setFieldInput:txtField.text];
                });
                
                [self dismissViewControllerAnimated:YES completion:nil];
            } else {
                [txtField setTextState:Error];
                [COIDefaults ShowAlert:@"Wrong Password" andMessage:@"Please enter valid password." withController:self];
            }
        }
        else if([titleString isEqualToString:@"Approval Code"]) {
            
            NSString *currentTimeStamp = [COIDefaults GetCurrentTimeStamp];
            NSMutableArray *signUpInputKeys = [[NSMutableArray alloc] initWithObjects:
                                               @"ApprovalCode",
                                               @"RequestTimeStamp",
                                               @"AppKey",
                                               nil];
            
            NSMutableDictionary *signUpRequestParametersDic=[[NSMutableDictionary alloc]initWithObjectsAndKeys:
                                                             txtField.text,@"ApprovalCode",
                                                             currentTimeStamp,@"RequestTimeStamp",
                                                             KAppKey,@"AppKey",
                                                             nil];
            
            
            NSLog(@"parameters are %@",signUpRequestParametersDic);
            
            NSString *hashParam=@"";
            for (NSInteger i=0; i<signUpInputKeys.count; i++) {
                hashParam = [hashParam stringByAppendingString:[signUpInputKeys objectAtIndex:i]];
                hashParam = [hashParam stringByAppendingString:@"="];
                hashParam = [hashParam stringByAppendingString:[signUpRequestParametersDic valueForKey:[signUpInputKeys objectAtIndex:i]]];
                if(i<signUpInputKeys.count-1){
                    hashParam = [hashParam stringByAppendingString:@"&"];
                }
            }
            NSLog(@"hashParam %@",hashParam);
            hashParam = [hashParam stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSString *result =[COIDefaults hmacWithKey:KPrivateKey andData:hashParam];
            
            [signUpRequestParametersDic setValue:@"" forKey:@"MallID"];
            [signUpRequestParametersDic setValue:result forKey:@"TokenID"];
            
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:signUpRequestParametersDic options:NSJSONWritingPrettyPrinted error:nil];
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@svc/svcHifi.svc/CheckApprovalCode",KWebServiceBaseURL]];
            
            NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
            configuration.timeoutIntervalForResource = kWebServiceTimeOutInterval;
            configuration.timeoutIntervalForRequest = kWebServiceTimeOutInterval;
            NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:kWebServiceTimeOutInterval];
            // Specify that it will be a POST request
            request.HTTPMethod = @"POST";
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            
            NSString *jsonstring =[[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSLog(@"json string is %@", jsonstring);
            request.HTTPBody = jsonData;
            
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
                if(data!=nil &&httpResponse.statusCode==200){
                    
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    NSLog(@"save customer response is %@",json);
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        if([[json valueForKey:@"ResponseCode"] isEqualToString:@"Y"]){

                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, KBasePopUpViewAnimationTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                [self.popUpDelegate setFieldInput:self->txtField.text];
                            });
                            
                            [self dismissViewControllerAnimated:YES completion:nil];
                            
                        }else{
//                            [txtField setTextState:Error];
                            [COIDefaults ShowAlert:@"Error" andMessage:[json valueForKey:@"ProcessResponse"] withController:self];
                        }
                    });
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        // Server Failure
                    });
                }
            }];
            [postDataTask resume];

//            [txtField setTextState:Error];
//            [COIDefaults ShowAlert:@"Wrong Approval Code" andMessage:@"Please enter correct approval code." withController:self];
        }
    }
}

#pragma mark UITextFieldDelegateMethods
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    IQKeyboardManager *keyboardManager = [IQKeyboardManager sharedManager];
    if (keyboardManager.canGoNext) {
        [keyboardManager goNext];
    } else {
        [keyboardManager resignFirstResponder];
    }
    return NO;
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;   // return NO to not change text
{
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    if([string length] == 0)
    {
        if([textField.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    if(textField.text.length==0 && [string isEqualToString:@" "])
    {
        return NO;
    }
    
    if(newLength>50)
    {
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if(textField == txtField && txtField.textState== Error){
        [txtField setTextState:normal];
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
