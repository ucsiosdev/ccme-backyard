//
//  COIDefaults.h
//  CityOfImagination
//
//  Created by Neha Gupta on 4/6/18.
//  Copyright © 2018 USHYAKU-IOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "COIConstant.h"

@interface COIDefaults : NSObject

+(void)ShowAlert:(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)viewController;
+(void)ShowConfirmationAlert:(NSString *)alertTitle andMessage :(NSString *) alertMessage andActions:(NSMutableArray *)actionsArray withController:(UIViewController *)viewController;

+ (NSString *)hmacWithKey:(NSString *)key andData:(NSString *)data;
+ (NSString*)base64forData:(NSData*)theData;

+ (BOOL) validateEmail:(NSString *) candidate;
+(NSString *)getValidStringValue:(id)inputstr;
+(NSString*)GetCurrentTimeStamp;
+(NSString *)getUTF8String:(NSString *)inputstring;

+(void)setApprovalCode:(NSString *)approvalCode;
+(NSString *)getApprovalCode;
+(void)setMallID:(NSString *)mallID;
+(NSString *)getMallID;
+(void)setUserID:(NSString *)mallID;
+(NSString *)getUserID;
+(void)setNationalityArray:(NSMutableArray *)array;
+(NSMutableArray *)getNationalityArray;

@end
