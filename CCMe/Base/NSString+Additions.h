//
//  NSString+Additions.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/8/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)

- (NSString *)base64String;
- (NSString *)sentenceCapitalizedString; // sentence == entire string
- (NSString *)realSentenceCapitalizedString; // sentence == real sentences
-(NSString *)urlencode;
+ (BOOL)isEmpty:(NSString *)string;
- (NSString *)floatString;
+ (NSString *)createGuid;
- (NSString *)trimString;
- (NSString *)AmountStringWithOutComas;
-(NSString *)percentageString;
+(NSString *)getValidRouteStringValue:(id)inputstr;
+(NSString *)getValidStringValue:(id)inputstr;
@end
