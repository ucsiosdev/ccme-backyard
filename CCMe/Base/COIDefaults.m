//
//  COIDefaults.m
//  CityOfImagination
//
//  Created by Neha Gupta on 4/6/18.
//  Copyright © 2018 USHYAKU-IOS. All rights reserved.
//

#import "COIDefaults.h"
#import <CommonCrypto/CommonHMAC.h>

@implementation COIDefaults

+(void)ShowAlert:(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)viewController{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(alertTitle,nil)
                                  message:NSLocalizedString([COIDefaults getValidStringValue:alertMessage], nil)
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:NSLocalizedString(KAlertOkButtonTitle,nil)
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    [alert addAction:ok];
    [viewController presentViewController:alert animated:YES completion:nil];
    
}
+(void)ShowConfirmationAlert:(NSString *)alertTitle andMessage :(NSString *) alertMessage andActions:(NSMutableArray *)actionsArray withController:(UIViewController *)viewController{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(alertTitle,nil)
                                  message:NSLocalizedString(alertMessage,nil)
                                  preferredStyle:UIAlertControllerStyleAlert];
    for (NSInteger i=0; i<actionsArray.count; i++) {
        [alert addAction:[actionsArray objectAtIndex:i]];
    }
    [viewController presentViewController:alert animated:YES completion:nil];
    
}

+ (BOOL) validateEmail:(NSString *) candidate {
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

+(NSString *)getValidStringValue:(id)inputstr
{
    @try {
        if ([inputstr isEqual:(id)[NSNull null]] || inputstr==nil)
        {
            return [[NSString alloc]init];
        }
        if([inputstr isKindOfClass:[NSString class]] && ([inputstr isEqualToString:@"<null>"] || [inputstr isEqualToString:@"<Null>"]) )
        {
            return [[NSString alloc]init];
        }
    }
    @catch (NSException *exception) {
        
        if([inputstr isKindOfClass:[NSNumber class]]){
            return [NSString stringWithFormat:@"%@",inputstr];
            
        }else if([inputstr isKindOfClass:[NSString class]]){
            return [NSString stringWithFormat:@"%@",inputstr];
            
        }else{
            return [NSString stringWithFormat:@"%@",inputstr];
        }
    }
    
    
    return [NSString stringWithFormat:@"%@",inputstr];
}

/*hmac**/
+ (NSString *)hmacWithKey:(NSString *)key andData:(NSString *)data
{
    const char *cKey = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [data cStringUsingEncoding:NSASCIIStringEncoding];
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    
    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    
    NSData *hash = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    NSString* s = [self base64forData:hash];
    
    return s;
}
+ (NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] = table[(value >> 18) & 0x3F];
        output[theIndex + 1] = table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6) & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0) & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

+(NSString*)GetCurrentTimeStamp
{
    NSDate *currentDate = [[NSDate alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    // or specifc Timezone: with name
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:timeZone];
    NSString *DateString = [dateFormatter stringFromDate:currentDate];
    return DateString;
}

+(NSString *)getUTF8String:(NSString *)inputstring
{
    NSCharacterSet *customCharacterset = [[NSCharacterSet characterSetWithCharactersInString:@"\"#%<>[\\]^`{|}+ =/ "] invertedSet];
    NSString *result = [inputstring stringByAddingPercentEncodingWithAllowedCharacters:customCharacterset];
    return result;
}

+(void)setApprovalCode:(NSString *)approvalCode{
    [[NSUserDefaults standardUserDefaults] setValue:approvalCode forKey:@"ApprovalCode"];
}

+(NSString *)getApprovalCode{
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"ApprovalCode"];
}

+(void)setMallID:(NSString *)mallID{
    [[NSUserDefaults standardUserDefaults] setValue:mallID forKey:@"MallID"];
}

+(NSString *)getMallID{
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"MallID"];
}

+(void)setUserID:(NSString *)mallID{
    [[NSUserDefaults standardUserDefaults] setValue:mallID forKey:@"UserID"];
}

+(NSString *)getUserID{
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];
}

+(void)setNationalityArray:(NSMutableArray *)array{
    [[NSUserDefaults standardUserDefaults] setValue:array forKey:@"NationalityData"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSMutableArray *)getNationalityArray{
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"NationalityData"];
}

@end
