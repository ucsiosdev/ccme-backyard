//
//  COINetworkManager.h
//  CityOfImagination
//
//  Created by Neha Gupta on 4/6/18.
//  Copyright © 2018 USHYAKU-IOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@interface COINetworkManager : NSObject

+(COINetworkManager*) retrieveManager;
-(BOOL)isNetworkReachable;

@end
