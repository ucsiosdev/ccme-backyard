//
//  COIDefaultSearchTableViewCell.h
//  CityOfImagination
//
//  Created by Neha Gupta on 4/6/18.
//  Copyright © 2018 USHYAKU-IOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface COIDefaultSearchTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@end
