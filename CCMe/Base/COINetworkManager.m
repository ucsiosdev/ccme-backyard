//
//  COINetworkManager.m
//  CityOfImagination
//
//  Created by Neha Gupta on 4/6/18.
//  Copyright © 2018 USHYAKU-IOS. All rights reserved.
//

#import "COINetworkManager.h"

@implementation COINetworkManager

static COINetworkManager *sharedSingleton = nil;

+ (COINetworkManager*) retrieveManager {
    @synchronized(self)
    {
        if (sharedSingleton == nil) {
            sharedSingleton = [[COINetworkManager alloc] init];
        }
    }
    return sharedSingleton;
}




-(BOOL)isNetworkReachable
{
    
    if ([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable ||
        [AFNetworkReachabilityManager sharedManager].networkReachabilityStatus == AFNetworkReachabilityStatusUnknown) {
        return NO;
    }
    else{
        return YES;
    }
}

@end
