//
//  COITextField.h
//  CityOfImagination
//
//  Created by Neha Gupta on 4/6/18.
//  Copyright © 2018 USHYAKU-IOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "COIConstant.h"

@interface COITextField : UITextField<UITextFieldDelegate>

@property (nonatomic,strong) IBInspectable NSString * ErrorString;
@property (nonatomic,strong,setter=setDropDownImage:) IBInspectable NSString *DropDownImage;
@property (strong,nonatomic) UIImageView *dropdownImageView;
@property (nonatomic,setter=setTextState:) TextfieldTextState textState;

@end
