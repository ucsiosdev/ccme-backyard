//
//  COIDefaultSearchViewController.h
//  CityOfImagination
//
//  Created by Neha Gupta on 4/6/18.
//  Copyright © 2018 USHYAKU-IOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "COIConstant.h"
#import "NSString+Additions.h"

@protocol ShopBahrainSearchSelectedObjectDelegate <NSObject>
-(void)selectedObject:(id)selectedItem;
@end

@interface COIDefaultSearchViewController : UIViewController <UISearchControllerDelegate,UISearchResultsUpdating, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>
{
    UISearchController *searchController;
    id searchDelegate;
    NSMutableArray * unfilteredArray;
    
    IBOutlet UILabel *titleLabel;
    
    NSMutableArray * selectedCellIndexPathsArray;
    NSMutableArray * selectedObjectsArrayforMultiSelect;
}
- (IBAction)doneButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *searchTableView;
@property(strong,nonatomic) NSMutableArray * contentArray;

@property(strong,nonatomic) id titleString;
@property(strong,nonatomic) id objectType;
@property(nonatomic) id searchDelegate;


@end
