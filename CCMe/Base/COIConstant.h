//
//  COIConstant.h
//  CityOfImagination
//
//  Created by Neha Gupta on 4/6/18.
//  Copyright © 2018 USHYAKU-IOS. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kSB_FONT_Medium(s) [UIFont fontWithName:@"Gotham-Medium" size:s]
#define TextFieldDescriptionLabelColor [UIColor colorWithRed:(234.0/255.0) green:(115.0/255.0) blue:(17.0/255.0) alpha:1]
#define TextFieldBackgroundColor [UIColor colorWithRed:(245.0/255.0) green:(245.0/255.0) blue:(245.0/255.0) alpha:1]
#define TextFieldBorderColor [UIColor colorWithRed:(187.0/255.0) green:(187.0/255.0) blue:(187.0/255.0) alpha:1]


#define KBasePopUpViewAnimationTime 0.5
#define kWebServiceTimeOutInterval 120.0

#define  KAlertOkButtonTitle @"Ok"


// Test Application

//#define KWebServiceBaseURL @"https://testecoupon.maf.ae/"
//#define KAppKey @"3d92272af69bba59bfa600b233e934605f255b5c5fc4251d37eb32fa1297d39a"
//#define KPrivateKey @"1ae62e36396109bf379788556159525c1a3afa70c67ea5fb5b1a63c7f7484573"


//Live Application

#define KWebServiceBaseURL @"https://ecoupon.maf.ae/"
#define KAppKey @"7d12042ae69cda12cdf205b213a391201f255b5c5fc5251c37ed32af1297e39c"
#define KPrivateKey @"4ca12f14326203af379778352153525d1a3afd70c67da5fa5b1a63c7f2185293"


typedef enum{
    Normal,
    Error,
} TextfieldTextState;

@interface COIConstant : NSObject

@end
