//
//  SettingViewController.h
//  Customer Registration
//
//  Created by Neha Gupta on 11/14/18.
//  Copyright © 2018 USHYAKU-IOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "COITextField.h"

@interface SettingViewController : UIViewController<UIPopoverPresentationControllerDelegate>
{
    IBOutlet COITextField *txtCSOAccount;
    IBOutlet COITextField *txtDefaultMall;
    
    NSMutableArray *arrayMallList;
}

@end
