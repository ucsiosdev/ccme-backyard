//
//  FormViewController.h
//  CityOfImagination
//
//  Created by Neha Gupta on 4/6/18.
//  Copyright © 2018 USHYAKU-IOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "COITextField.h"

@interface FormViewController : UIViewController<UIPopoverPresentationControllerDelegate>
{
    IBOutlet COITextField *txtTitle;
    IBOutlet COITextField *txtFirstName;
    IBOutlet COITextField *txtLastName;
    IBOutlet COITextField *txtResidentialStatus;
    IBOutlet COITextField *txtNationality;
    IBOutlet COITextField *txtCountryOfResidence;
    IBOutlet COITextField *txtCity;
    IBOutlet COITextField *txtAreaOfResidence;
    IBOutlet COITextField *txtCountryCode;
    IBOutlet COITextField *txtMobileNumber;
    IBOutlet COITextField *txtEmailAddress;
    
    NSString *objectType;
    
    NSMutableArray *titleDicsArray;
    NSMutableArray *nationalitiesDataArray;
    NSMutableArray *residentialArray;
    NSMutableArray *residentialCityArray;
    NSMutableArray *residentialAreaArray;

    
    IBOutlet UIButton *btnSubmit;
    IBOutlet NSLayoutConstraint *xFormViewTopConstraint;
}
@end
