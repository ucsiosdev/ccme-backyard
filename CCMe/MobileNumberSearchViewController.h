//
//  MobileNumberSearchViewController.h
//  CCMe
//
//  Created by appledev on 18/02/19.
//  Copyright © 2019 appledev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "COITextField.h"

NS_ASSUME_NONNULL_BEGIN

@interface MobileNumberSearchViewController : UIViewController<UIPopoverPresentationControllerDelegate>
{
    NSMutableArray *nationalitiesDataArray;
    
    IBOutlet COITextField *countryCodeTextField;
    IBOutlet COITextField *mobileNumberTextField;
    NSString *objectType;
}
@end

NS_ASSUME_NONNULL_END
