//
//  HomeViewController.m
//  CCMe
//
//  Created by appledev on 18/02/19.
//  Copyright © 2019 appledev. All rights reserved.
//

#import "HomeViewController.h"
#import "COIDefaults.h"
#import "COIDefaultPopUpViewController.h"
#import "SettingViewController.h"
#import "MobileNumberSearchViewController.h"
@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)startButtonTapped:(id)sender {
    
    if ([COIDefaults getMallID].length == 0 || [COIDefaults getApprovalCode].length == 0) {
        [COIDefaults ShowAlert:@"Missing Data" andMessage:@"Please update settings first." withController:self];
    } else {
        
        MobileNumberSearchViewController *formVC = [[MobileNumberSearchViewController alloc]initWithNibName:@"MobileNumberSearchViewController" bundle:nil];
        [self.navigationController pushViewController:formVC animated:YES];
        
    }
}
- (IBAction)settingButtonTapped:(UIButton *)sender {
    COIDefaultPopUpViewController *defaultPopUpVC = [[COIDefaultPopUpViewController alloc]init];
    defaultPopUpVC.popUpDelegate = self;
    defaultPopUpVC.titleString = @"Setting Password";
    
    UINavigationController * baNavController=[[UINavigationController alloc]initWithRootViewController:defaultPopUpVC];
    baNavController.preferredContentSize=CGSizeMake(376, 200);
    baNavController.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *presentationController = baNavController.popoverPresentationController;
    presentationController.delegate=self;
    presentationController.sourceView = sender;
    presentationController.sourceRect = CGRectMake(0, 0, sender.frame.size.width, sender.frame.size.height);
    presentationController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    
    [self presentViewController:baNavController animated:YES completion:^{
        presentationController.passthroughViews=nil;
        self.modalInPopover=YES;
        
    }];
}

#pragma mark PopUp delegate method
-(void)setFieldInput:(NSString *)text
{
    if ([text isEqualToString:@"@b@cu$"]) {
        SettingViewController *settingVC = [[SettingViewController alloc]initWithNibName:@"SettingViewController" bundle:nil];
        [self.navigationController pushViewController:settingVC animated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
